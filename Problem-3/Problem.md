# Problem 3

The prime factors of $13195$ are $5$, $7$, $13$ and $29$. What is the largest prime factor of the number $600851475143$?

## Solution

Grab the number and check if it is prime. If it isn't, evenly divide by a prime (start with $2$) all the possible times. Then search for the next prime and continue until you get 1 or a prime number. In the first case, the largest prime factor is the last number you divided by. In the second case, that prime number you get is the largest prime factor.
