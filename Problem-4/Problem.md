# Problem 4

A palindromic number reads the same both ways. The largest palindrome made from the product of two $2$-digit numbers is $91\times99=9009$. Find the largest palindrome made from the product of two $3$-digit numbers.

## Solution

Just applied brute force to evaluate almost all possible products.
